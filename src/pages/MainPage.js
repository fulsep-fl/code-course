import React from 'react'
import styled from 'styled-components'

const Root = styled.div`
  display:flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
`

const Sidebar = styled.div`
  display: flex;
  width: 25px;
  background: #1d2750;
  border: 2px solid #fff;
  color: #fff;
  justify-content: center;
  padding-top: 10px;
  & span{
    cursor: pointer;
    user-select: none;
  }
`
const Titlebar = styled.div`
  display: flex;
  align-items: center;
  height:25px;
  padding-left: 30px;
  border: 2px solid #fff;
  background: #1d2750;
  color: #fff;
`

const Container = styled.div`
  display:flex;
  flex:1;
`

const Content = styled.div`
    display: flex;
    flex:1;
    ${props=>props['data-direction']==='row'?`
      flex-direction:column;
    `:''}
`
const Col = styled.div`
    display: flex;
    flex:1;
    color: #fff;
    padding: ${props=>props.padding||'5px'};
    font-family: monospace;
    ${props=>props['data-direction']==='row'?`
      flex-direction:column;
    `:''}
    ${props=>props['data-bg']?`
      background-color: ${props['data-bg']}
    `:`
      background-color: #000;
    `}
    ${props=>props['border']?`
      border: 2px solid #fff;
    `:`
        
    `}
`

const Navigator = styled.div`
    font-family: Arial;
    font-weight: bold;
    font-size: 14px;
    text-transform: uppercase;
    align-self:flex-end;
    width: 100%;
    background: #1d2750;
    color: #fff;
    text-align: right;
    & span{
      user-select: none;
      cursor: pointer;
      display: inline-block;
      margin: 5px 100px;
    }
`

const Text = styled.div`
    color: ${props=>props.color||'#b05608'};
    font-weight: bold;
    &:before{
      content: '>> ';
    }
`

export default function MainPage() {
  const menuAct = ()=>{
    window.alert('Menu Click Action')
  }
  const resetCode = ()=>{
    window.alert('Code Reset Action');
  }
  const nextAct = ()=>{
    window.alert('Next Action');
  }
  return (
    <Root>
      <Titlebar>
        <span>File</span>
      </Titlebar>
      <Container>
        <Sidebar>
          <span onClick={menuAct}>||}</span>
        </Sidebar>
        <Content data-direction='row'>
          <Content>
            <Col border data-direction='row' padding='0'>
              <Col />
              <Navigator>
                <span onClick={resetCode}>Reset Code</span>
                <span onClick={nextAct}>Next</span>
              </Navigator>
            </Col>
            <Col border data-direction='row'>
                <Text>This is where your instructions will be!</Text>
                <Text/>
                <Text>Course Loaded!</Text>
                <Text>To view your courses, click on the '||}' button on the left side of the window</Text>
                <Text>To start working on a course, click on desired course.</Text>
            </Col>
          </Content>
          <Content>
            <Col border>
              <Text color='#fff'>This is your output code</Text>
            </Col>
          </Content>
        </Content>
      </Container>
    </Root>
  )
}
